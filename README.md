Programming exercise: File Comparison

Write a C program to compare 2 binary files

# Instructions

1. Provide two binary files you want to compare as command line arguments 
2. Modify the BUFFER_SIZE by changing the value of the macro in the beginning of the file
3. Run `main.c` and view the results of the output in the log file
4. Sample binary files are provided in the `bin` folder

# Results

These are the values I observed while reading a 1 MB file

![Runtime values](file_readtime.png)
