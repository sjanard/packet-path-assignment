#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include<fcntl.h>
#include<time.h> 

// Edit block size to be read here
#define BUFFER_SIZE 8192

int min(int a, int b)
{
    return (a > b) ? b : a;
}

int main(int argc, char *argv[])
{

    if(argc > 3)
    {
        printf("Enter only 2 files\n");
        exit (1);
    }

    // Initialising file pointers, buffer for each file and extra variables
    FILE *fptr1, *fptr2, *fptrw;

    fptr1 = fopen(argv[1],"rb");
    fptr2 = fopen(argv[2], "rb");
    fptrw = fopen("logs.txt", "w");

    char buffer[BUFFER_SIZE], buffer2[BUFFER_SIZE];
    int bytes_read = 0, bytes_read2 = 0;
    int blocksread = 0;

    int difference_flag = 1;
    clock_t t = clock();

    if(fptr1 != NULL && fptr2 != NULL)
    {   
        while ((bytes_read = fread(buffer, 1, sizeof(buffer), fptr1)) > 0 && (bytes_read2 = fread(buffer2, 1,sizeof(buffer2), fptr2)))
        {
            
            if(memcmp(buffer, buffer2, BUFFER_SIZE) != 0)
            {
                difference_flag = 0;
                break;
            }
            printf("Bytes Read: %d  %d\tBlocksread: %d\n", bytes_read, bytes_read2, blocksread);
            blocksread++;
        }
    }
    else
    {
        printf("Unable to open the files\n. Please enter a valid file name or file location");
        exit (1);
    }

    // Binary search performed on the buffer  if there is a difference between the two files

    int i = 0, left = 0, right = min(bytes_read, bytes_read2);
    int mid;

    while(left < right)
    {
        mid = (left + (right - left) / 2);

        if( buffer[mid] != buffer2[mid] )
            right = mid;
        else
            left = mid + 1;
    }

    right = min(bytes_read, bytes_read2);
    t = clock() - t;
    double time_taken = ((double)t / CLOCKS_PER_SEC) * 1000;


    fprintf(fptrw, "Time taken to compare the two files = %fms\n", time_taken);
    fprintf(fptrw, "Number of blocks read = %d, BLOCK_SIZE = %d, Offset = %d\n", blocksread, BUFFER_SIZE, left);

    if(!difference_flag)
    {
        fprintf(fptrw, "Value1\tValue2\n");
        for(i = left; i < min(left + 16, right) ; i++)
        {
            fprintf(fptrw, "%x\t%x\n",buffer[i],buffer2[i]);
        }
    }
    
    exit (0);

}